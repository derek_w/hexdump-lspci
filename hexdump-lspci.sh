#!/bin/bash

PrintHelp()
{
cat <<EOM
   -------------------------------------------------------------------------
      Usage: $(basename $0)  [-r] [-d] [-v BUILD_VERSION]

         -h       Print [h]elp and exit.
         -s       Specify PCI device "-s 00:01.2" to dump
   -------------------------------------------------------------------------
EOM

   exit 0
}

declare ALL_CONFIG_PATH="/sys/bus/pci/devices/*/config"
declare LSPCI_DUMP_FILE="dump.txt"

declare DEVICE_NUMBER=""
declare CONFIG_FILE_PATH=""

while getopts hs: opt
do
   case "$opt" in
     h) PrintHelp;;
     s) DEVICE_NUMBER=$OPTARG;;
   esac
done

if [ -f "$LSPCI_DUMP_FILE" ]; then
   echo "Removing previous dump file"
   rm $LSPCI_DUMP_FILE
fi


if [ "$DEVICE_NUMBER" != "" ]; then
   CONFIG_FILE_PATH="/sys/bus/pci/devices/0000:${DEVICE_NUMBER}/config"
fi


if [ "$CONFIG_FILE_PATH" != "" ]; then
   echo "Dumping Configuration space of $DEVICE_NUMBER to $LSPCI_DUMP_FILE"

   echo $CONFIG_FILE_PATH | grep -oE '....\:..\:..\..' | tr -d '\n' >> $LSPCI_DUMP_FILE
   hexdump -n 2 -s 10 -e '/2 " %04x"' ${CONFIG_FILE_PATH} >> $LSPCI_DUMP_FILE
   echo -ne ": " >> $LSPCI_DUMP_FILE
   hexdump -n 2 -s 0 -e '/2 "%04x"' ${CONFIG_FILE_PATH} >> $LSPCI_DUMP_FILE
   echo -ne ":" >> $LSPCI_DUMP_FILE
   hexdump -n 2 -s 2 -e '/2 "%04x\n"' ${CONFIG_FILE_PATH} >> $LSPCI_DUMP_FILE
   hexdump -n 256 -v -e '"%02_ax: " 16/1 "%02x " "\n"' $CONFIG_FILE_PATH >> $LSPCI_DUMP_FILE
   echo -ne "\n" >> $LSPCI_DUMP_FILE
else
   echo  "Dumping configuration space of the following devices to $LSPCI_DUMP_FILE"
   for f in $ALL_CONFIG_PATH; do
      echo $f | grep -oE '....\:..\:..\..'
   done

   for f in $ALL_CONFIG_PATH; do
      echo $f | grep -oE '....\:..\:..\..' | tr -d '\n' >> $LSPCI_DUMP_FILE
      hexdump -n 2 -s 10 -e '/2 " %04x"' ${f} >> $LSPCI_DUMP_FILE
      echo -ne ": " >> $LSPCI_DUMP_FILE
      hexdump -n 2 -s 0 -e '/2 "%04x"' ${f} >> $LSPCI_DUMP_FILE
      echo -ne ":" >> $LSPCI_DUMP_FILE
      hexdump -n 2 -s 2 -e '/2 "%04x\n"' ${f} >> $LSPCI_DUMP_FILE
      hexdump -n 256 -v -e '"%02_ax: " 16/1 "%02x " "\n"' $f >> $LSPCI_DUMP_FILE
      echo -ne "\n" >> $LSPCI_DUMP_FILE
   done
fi

